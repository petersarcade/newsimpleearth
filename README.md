# README #

This is the config for Peter's spigot server with earth map and dynmap server.

### Docker Server ###
* [Minecraft Server](https://hub.docker.com/r/itzg/minecraft-server)

### World Data ###

* [Minecraft Earth Map](https://earth.motfe.net/2019/01/02/downloads/)

### Plugins ###

* [Dynmap](https://www.spigotmc.org/resources/dynmap.274/)

### Crontab ###
```
# NOTE: Times are in GMT
# m h  dom mon dow   command
55 5 * * 1-5 ~/newsimpleearth/shutdown_warning.sh >> /tmp/cron.log 2>&1
0 6 * * 1-5 ~/newsimpleearth/shutdown.sh >> /tmp/cron.log 2>&1
0 11 * * 1-5 ~/newsimpleearth/startup.sh >> /tmp/cron.log 2>&1
```
